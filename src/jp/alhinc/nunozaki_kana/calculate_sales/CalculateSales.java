package jp.alhinc.nunozaki_kana.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;



public class CalculateSales {
	public static void main(String[] args) {

		HashMap<String, String> namemap = new HashMap<String, String>();

		HashMap<String, Long> salesmap = new HashMap<String, Long>();

		BufferedReader br = null;

		//コマンドライン引数が1以外のときのエラー処理
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		try {
			File file = new File(args[0], "branch.lst");

			//支店定義ファイルが存在しないときのエラー処理
			if(!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}

			FileReader fr = new FileReader(file);

			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null){
				String [] split = line.split(",");

				//支店定義ファイル1行の要素数が2つ以外の場合のエラー処理
				if(split.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}

				//支店コードのフォーマットが不正の場合のエラー処理
				if(!split[0].matches("^[0-9]{3}$")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}

				namemap.put(split[0], split[1]);
				salesmap.put(split[0],0L);


			}

		} catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
				}
			}
		}


		File dir = new File(args[0]);

		File[] list = dir.listFiles();

		ArrayList<File> rcdlist = new ArrayList<File>();



		if(list != null) {
			//ディレクトリの中のファイルの件数分ループ
			for(int i=0; i<list.length; i++) {
				// 売り上げファイルの場合
				if(list[i].isFile() && list[i].getName().matches("^[0-9]{8}.rcd$")) {

					rcdlist.add(list[i]);
				}
			}
		}

		//連番チェック
		for(int i=0; i<rcdlist.size()-1; i++) {
			//売上ファイルが歯抜けになっている場合
			int number = Integer.parseInt(rcdlist.get(i).getName().substring(0,8));
			int nextnumber = Integer.parseInt(rcdlist.get(i+1).getName().substring(0,8));
			if(nextnumber - number != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}

		}


			//rcdlistの件数分ループ
		for(int i=0; i<rcdlist.size(); i++) {
			try {
				File rcdfile = new File(args[0], list[i].getName());
				FileReader rcdfr = new FileReader(rcdfile);
				br = new BufferedReader(rcdfr);


				ArrayList<String> saleslist = new ArrayList<String>();

				//売り上げファイルを読み込む
				String rcdline;
				while((rcdline = br.readLine()) != null) {


					saleslist.add(rcdline);

				}

				//売上ファイルの支店コードが支店定義ファイルに存在しない場合のエラー処理
				if(!namemap.containsKey(saleslist.get(0))) {
					System.out.println(rcdlist.get(i).getName()+ "の支店コードが不正です");
					return;
				}

				//売上ファイルの行数が2行以外の場合のエラー処理
				if(saleslist.size() != 2) {
					System.out.println(rcdlist.get(i).getName() + "のフォーマットが不正です");
					return;
				}


				//売上金額に数字以外が含まれている場合のエラー処理
				if(!saleslist.get(1).matches ("^[0-9]+$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				//String型からLong型に変換
				long lon = Long.parseLong(saleslist.get(1));


				//売上額の加算
				Long amount;
				amount = salesmap.get(saleslist.get(0)) + lon;


				salesmap.put(saleslist.get(0),amount);


				//支店別集計合計金額が10桁超えた場合のエラー処理
				if(amount >= 10000000000L ) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
			} finally {
				if(br != null) {
					try {
						br.close();
					}catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
					}
				}
			}
		}


		BufferedWriter bw = null;
		try {
			//支店別集計ファイルを作成,出力
			File bofile = new File(args[0],"branch.out");
			FileWriter filewriter = new FileWriter(bofile);
			bw = new BufferedWriter(filewriter);

			for(Map.Entry<String,String> entry : namemap.entrySet()) {
				bw.write(entry.getKey() + "," + entry.getValue() + "," + salesmap.get(entry.getKey()));
				bw.newLine();
			}

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
				}
			}
		}
	}
}
